//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 分页查询安保路线
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllAblx = (paramsData) => {
  return axios.get('/USSAC/queryAblx', {
    params: paramsData
  })
}

/**
 * 查询单条安保路线信息
 * @param {Number} id 编号
 * @returns
 */
export const selectOneAblx = (id) => {
  return axios.get('/USSAC/getByIdAblx', {
    params: { id: id }
  })
}

/**
 * 分页查询安保路线
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllJqzs = (paramsData) => {
  return axios.get('/USSAC/queryAllAlarm', {
    params: paramsData
  })
}

/**
 * 查询安保人员详情
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllAbry = (paramsData) => {
  return axios.get('/USSAC/querySecuritypersonnel', {
    params: paramsData
  })
}

/**
 * 查询警车对象详情
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllJcdx = (paramsData) => {
  return axios.get('/USSAC/selectJdchphmPV', {
    params: paramsData
  })
}

/**
 * 查询执勤民警详情
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllZqmj = (paramsData) => {
  return axios.get('/USSAC/selectPolice', {
    params: paramsData
  })
}
// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

// /**
//  * 新增参会人员数据
//  * @param {Object} Participant 新增数据
//  * @returns
//  */
// export const insert = (Participant) => {
//   return axios.post('/datum/insertParticipant', Participant)
// }

// /**
//  * 修改参会人员数据
//  * @param {Object} Participant 参会人员数据实体
//  * @returns
//  */
// export const update = (Participant) => {
//   return axios.put('/datum/updateParticipant', Participant)
// }

// /**
//  * 删除涉会车辆数据
//  * @param {Number} idList id集合
//  * @returns
//  */
// export const delData = (idList) => {
//   return axios.delete(`/datum/delParticipant?idList=${idList}`)
// }
