//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 查询所有系统角色
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/sysUser/selectAllRoles', {
    params: paramsData
  })
}

/**
 * 新增系统角色
 * @param {Object} sysRoles 新增数据
 * @returns
 */
export const insert = (sysRoles) => {
  return axios.post('/sysUser/insertRoles', sysRoles)
}

export function getList() {
  return axios.get('/sysUser/selectAllMenu')
}
/**
 * @param {Object} sysMenu 修改的系统菜单
 * @returns
 */
export function addAndupdateRoles(sysRolesMenu) {
  return axios.post('/sysUser/addAndupdateRoles', sysRolesMenu)
}
/**
 * 修改系统角色
 * @param {Object} sysRoles 修改数据
 * @returns
 */
export const update = (sysRoles) => {
  return axios.put('/sysUser/updateRoles', sysRoles)
}

/**
 * 删除系统角色
 * @param {Number} roleId 角色编号
 * @returns
 */
export const delData = (roleId) => {
  return axios.delete(`/sysUser/deleteRoles?idList=${roleId}`)
}
// /**
//  * @param {Object} sysMenu 新增的系统菜单
//  * @returns
//  */
// export function insert(sysMenu) {
//   return axios.post('/sysUser/insertMenu', sysMenu)
// }
