//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 查询所有系统用户
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/sysUser/selectAll', {
    params: paramsData
  })
}

/**
 * 新增系统用户
 * @param {Object}  sysUser 新增系统用户数据
 * @returns
 */
export const insert = (sysUser) => {
  return axios.post('/sysUser/insert', sysUser)
}

/**
 * 修改系统用户
 * @param {Object} sysUser 修改系统用户数据
 * @returns
 */
export const update = (sysUser) => {
  return axios.put('/sysUser/update', sysUser)
}

/**
 * 删除系统角色
 * @param {Number} userId 用户id
 * @returns
 */
export const delData = (userId) => {
  return axios.delete(`/sysUser/delete?idList=${userId}`)
}
// /**
//  * @param {Object} sysMenu 新增的系统菜单
//  * @returns
//  */
// export function insert(sysMenu) {
//   return axios.post('/sysUser/insertMenu', sysMenu)
// }
