//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询布控车辆基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/controlManagement/selectClbkByLimit', {
    params: paramsData
  })
}

/**
 * 新增布控车辆数据
 * @param {Object} clbk 新增数据
 * @returns
 */
export const insert = (clbk) => {
  return axios.post('/controlManagement/insertClbk', clbk)
}

/**
 * 修改布控车辆数据
 * @param {Object} clbk 重点车辆位置数据
 * @returns
 */
export const update = (clbk) => {
  return axios.put('/controlManagement/updateClbk', clbk)
}

/**
 * 删除单条布控车辆数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/controlManagement/deleteClbk/${idList}`)
}
/**
 * 查询单条布控车辆数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/controlManagement/selectClbkById', {
    params: { id: id }
  })
}
