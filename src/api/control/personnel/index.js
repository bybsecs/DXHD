//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询短信设置基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/controlManagement/selectDxszByLimit', {
    params: paramsData
  })
}

/**
 * 新增短信设置数据
 * @param {Object} dxsz 新增数据
 * @returns
 */
export const insert = (dxsz) => {
  return axios.post('/controlManagement/insertDxsz', dxsz)
}

/**
 * 修改短信设置数据
 * @param {Object} dxsz 重点车辆位置数据
 * @returns
 */
export const update = (dxsz) => {
  return axios.put('/controlManagement/updateDxsz', dxsz)
}

/**
 * 删除单条短信设置数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/controlManagement/deleteDxsz/${idList}`)
}
/**
 * 查询单条短信设置数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/controlManagement/selectDxszById', {
    params: { id: id }
  })
}

/**
 * 查询公安机关信息api
 * @param
 * @returns
 */
export const selectAllGajg = () => {
  return axios.get('/activity/selectAllGajg')
}
