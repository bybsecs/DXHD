//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询布控人员基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/controlManagement/selectRybkByLimit', {
    params: paramsData
  })
}

/**
 * 新增布控人员数据
 * @param {Object} rybk 新增数据
 * @returns
 */
export const insert = (rybk) => {
  return axios.post('/controlManagement/insertRybk', rybk)
}

/**
 * 修改布控人员数据
 * @param {Object} rybk 重点人员数据
 * @returns
 */
export const update = (rybk) => {
  return axios.put('/controlManagement/updateRybk', rybk)
}

/**
 * 删除单条布控人员数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/controlManagement/deleteRybk/${idList}`)
}
/**
 * 查询单条布控人员数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/controlManagement/selectRybkById', {
    params: { id: id }
  })
}
