//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询布控区域基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/controlManagement/selectQybkByLimit', {
    params: paramsData
  })
}

/**
 * 新增布控区域数据
 * @param {Object} qybk 新增数据
 * @returns
 */
export const insert = (qybk) => {
  return axios.post('/controlManagement/insertQybk', qybk)
}

/**
 * 修改布控区域数据
 * @param {Object} qybk 重点区域数据
 * @returns
 */
export const update = (qybk) => {
  return axios.put('/controlManagement/updateQybk', qybk)
}

/**
 * 删除单条布控区域数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/controlManagement/deleteQybk/${idList}`)
}
/**
 * 查询单条布控区域数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/controlManagement/selectQybkById', {
    params: { id: id }
  })
}
