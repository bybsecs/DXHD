//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 解控布控车辆api
 * @param {Object} map 解控数据
 * @returns
 */
export const removeClbk = (map) => {
  return axios.put('/controlManagement/removeClbk', map)
}

/**
 * 解控布控人员api
 * @param {Object} map 解控数据
 * @returns
 */
export const removeRybk = (map) => {
  return axios.put('/controlManagement/removeRybk', map)
}

/**
 * 解控布控区域api
 * @param {Object} map 解控数据
 * @returns
 */
export const removeQybk = (map) => {
  return axios.put('/controlManagement/removeQybk', map)
}

/**
 * 查询布控车辆基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllCar = (paramsData) => {
  return axios.get('/controlManagement/selectClbkByLimit', {
    params: paramsData
  })
}

/**
 * 查询布控人员基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllBkry = (paramsData) => {
  return axios.get('/controlManagement/selectRybkByLimit', {
    params: paramsData
  })
}
/**
 * 查询布控区域基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllBkqy = (paramsData) => {
  return axios.get('/controlManagement/selectQybkByLimit', {
    params: paramsData
  })
}
