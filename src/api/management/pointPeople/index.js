//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询所有重点人员
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectFocuspersonByLimit', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

/**
 * 新增重点人员数据
 * @param {Object} focusperson 新增数据
 * @returns
 */
export const insert = (focusperson) => {
  return axios.post('/datum/insertFocusperson', focusperson)
}

/**
 * 修改重点人员数据
 * @param {Object} focusperson 重点人员数据实体
 * @returns
 */
export const update = (focusperson) => {
  return axios.put('/datum/updateFocusperson', focusperson)
}

/**
 * 删除重点人员数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/deleteFocusperson/${idList}`)
}
/**
 * 查询单条重点人员
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get(`/datum/selectFocuspersonById/${id}`)
}
