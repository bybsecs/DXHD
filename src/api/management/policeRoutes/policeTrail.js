//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询安保路线轨迹基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectAblxgjByLimit', {
    params: paramsData
  })
}

/**
 * 新增安保路线轨迹数据
 * @param {Object} Ablxgj 新增数据
 * @returns
 */
export const insert = (Ablxgj) => {
  return axios.post('/datum/insertAblxgj', Ablxgj)
}

/**
 * 修改安保路线轨迹数据
 * @param {Object} Ablxgj 安保路线轨迹数据
 * @returns
 */
export const update = (Ablxgj) => {
  return axios.put('/datum/updateAblxgj', Ablxgj)
}

/**
 * 删除单条安保路线轨迹数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/deleteAblxgj/${idList}`)
}

// /**
//  * 查询单条安保路线轨迹数据
//  * @param {Number} id 编号
//  * @returns
//  */
// export const selectOne = (id) => {
//   return axios.get('/datum/selectAblxById', {
//     params: { id: id }
//   })
// }
