//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询重点车辆位置基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectZdclwzxxByLimit', {
    params: paramsData
  })
}

/**
 * 新增重点车辆位置数据
 * @param {Object} zdclwzxx 新增数据
 * @returns
 */
export const insert = (zdclwzxx) => {
  return axios.post('/datum/insertZdclwzxx', zdclwzxx)
}

/**
 * 修改重点车辆位置数据
 * @param {Object} zdclwzxx 重点车辆位置数据
 * @returns
 */
export const update = (zdclwzxx) => {
  return axios.put('/datum/updateZdclwzxx', zdclwzxx)
}

/**
 * 删除单条重点车辆位置数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/deleteZdclwzxx/${idList}`)
}
/**
 * 查询单条重点车辆位置数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectZdclwzxxById', {
    params: { id: id }
  })
}
