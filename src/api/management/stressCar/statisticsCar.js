//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询重点车辆统计基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectZdcltjxxbByLimit', {
    params: paramsData
  })
}

/**
 * 新增重点车辆统计数据
 * @param {Object} zdcltjxxb 新增数据
 * @returns
 */
export const insert = (zdcltjxxb) => {
  return axios.post('/datum/insertZdcltjxxb', zdcltjxxb)
}

/**
 * 修改重点车辆统计数据
 * @param {Object} zdcltjxxb 重点车辆统计数据
 * @returns
 */
export const update = (zdcltjxxb) => {
  return axios.put('/datum/updateZdcltjxxb', zdcltjxxb)
}

/**
 * 删除单条重点车辆统计数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/deleteZdcltjxxb/${idList}`)
}
/**
 * 查询单条重点车辆统计数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectZdcltjxxbById', {
    params: { id: id }
  })
}
