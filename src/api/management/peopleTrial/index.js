//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 分页查询所有人员背审结果
 * @param {Object} paramsData 分页数据
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/queryAllBackgroundinfo', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

/**
 * 新增人员背审
 * @param {Object} Backgroundinfo 新增数据
 * @returns
 */
export const insert = (Backgroundinfo) => {
  return axios.post('/datum/insertBackgroundinfo', Backgroundinfo)
}

/**
 * 修改人员背审
 * @param {Object} Backgroundinfo 涉会车辆数据实体
 * @returns
 */
export const update = (Backgroundinfo) => {
  return axios.put('/datum/updateBackgroundinfo', Backgroundinfo)
}

/**
 * 删除人员背审
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/delBackgroundinfo?idList=${idList}`)
}
/**
 * 查询单条人员背审结果
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectOneBackgroundinfo', {
    params: { id: id }
  })
}
