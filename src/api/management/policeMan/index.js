//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询所有安保人员信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/queryAllSecuritypersonnel', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

/**
 * 新增安保人员信息
 * @param {Object} Securitypersonnel 新增数据
 * @returns
 */
export const insert = (Securitypersonnel) => {
  return axios.post('/datum/insertSecuritypersonnel', Securitypersonnel)
}

/**
 * 修改安保人员信息
 * @param {Object} Securitypersonnel 安保人员信息实体
 * @returns
 */
export const update = (Securitypersonnel) => {
  return axios.put('/datum/updateSecuritypersonnel', Securitypersonnel)
}

/**
 * 删除安保人员信息
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/delSecuritypersonnel/${idList}`)
}
/**
 * 查询单条安保人员信息
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectOneSecuritypersonnel', {
    params: { id: id }
  })
}
