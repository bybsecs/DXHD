//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询所有涉会车俩
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/queryAllParticipantvehicle', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

/**
 * 新增涉会车辆数据
 * @param {Object} Participantvehicle 新增数据
 * @returns
 */
export const insert = (Participantvehicle) => {
  return axios.post('/datum/insertParticipantvehicle', Participantvehicle)
}

/**
 * 修改涉会车辆数据
 * @param {Object} Participantvehicle 涉会车辆数据实体
 * @returns
 */
export const update = (Participantvehicle) => {
  return axios.put('/datum/updateParticipantvehicle', Participantvehicle)
}

/**
 * 删除涉会车辆数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/delParticipantvehicle?idList=${idList}`)
}
/**
 * 查询单条涉会车辆数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectOneParticipantvehicle', {
    params: { id: id }
  })
}
