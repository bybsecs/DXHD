//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询民警基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectPoliceByLimit', {
    params: paramsData
  })
}

/**
 * 新增民警数据
 * @param {Object} police 新增数据
 * @returns
 */
export const insert = (police) => {
  return axios.post('/datum/insertPolice', police)
}

/**
 * 修改民警数据
 * @param {Object} police 民警数据实体
 * @returns
 */
export const update = (police) => {
  return axios.put('/datum/updatePolice', police)
}

/**
 * 删除民警数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/deletePolice/${idList}`)
}
/**
 * 查询单条民警数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectPoliceById/{id}', {
    params: { id: id }
  })
}
