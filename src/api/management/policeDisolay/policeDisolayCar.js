//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询警车基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectPVByLimit', {
    params: paramsData
  })
}

/**
 * 新增警车数据
 * @param {Object} Policevehicle 新增数据
 * @returns
 */
export const insert = (Policevehicle) => {
  return axios.post('/datum/savePolicevehicle', Policevehicle)
}

/**
 * 修改警车数据
 * @param {Object} Policevehicle 警车数据实体
 * @returns
 */
export const update = (Policevehicle) => {
  return axios.put('/datum/updatePolicevehicle', Policevehicle)
}

/**
 * 删除警车数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/delPolicevehicle/${idList}`)
}
/**
 * 查询警车数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/getIdPolicevehicle', {
    params: { id: id }
  })
}
