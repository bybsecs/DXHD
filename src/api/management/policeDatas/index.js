//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询警情基本数据
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/datum/selectAlarmByLimit', {
    params: paramsData
  })
}

/**
 * 新增警情数据
 * @param {Object} alarm 新增数据
 * @returns
 */
export const insert = (alarm) => {
  return axios.post('/datum/insertAlarm', alarm)
}

/**
 * 修改警情数据
 * @param {Object} alarm 警情数据实体
 * @returns
 */
export const update = (alarm) => {
  return axios.put('/datum/updateAlarm', alarm)
}

/**
 * 删除警情数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/deleteAlarm/${idList}`)
}
/**
 * 查询单条警情数据
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectAlarmById', {
    params: { id: id }
  })
}
