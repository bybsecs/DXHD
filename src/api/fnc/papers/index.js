//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 证件数据查询所有信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/certificateinfo/selectAll', {
    params: paramsData
  })
}

/**
 * 新增证件数据数据
 * @param {Object} fpxx 新增数据
 * @returns
 */
export const insert = (fpxx) => {
  return axios.post('/certificateinfo/insert', fpxx)
}

/**
 * 修改证件数据数据
 * @param {Object} fpxx 修改的数据
 * @returns
 */
export function update(fpxx) {
  return axios.put('/certificateinfo/update', fpxx)
}

/**
 * 删除证件数据数据
 * @param {String} xxzjbh 信息主键编号
 * @returns
 */
export const delData = (xxzjbh) => {
  return axios.delete(`/certificateinfo/delete?idList=${xxzjbh}`)
}
