//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 票务数据查询所有信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/fpxx/selectAll', {
    params: paramsData
  })
}

/**
 * 新增票务数据数据
 * @param {Object} fpxx 新增数据
 * @returns
 */
export const insert = (fpxx) => {
  return axios.post('/fpxx/insert', fpxx)
}

/**
 * 修改票务数据数据
 * @param {Object} fpxx 修改的数据
 * @returns
 */
export function update(fpxx) {
  return axios.put('/fpxx/update', fpxx)
}

/**
 * 删除票务数据数据
 * @param {String} xxzjbh 信息主键编号
 * @returns
 */
export const delData = (xxzjbh) => {
  return axios.delete(`/fpxx/delete?idList=${xxzjbh}`)
}
