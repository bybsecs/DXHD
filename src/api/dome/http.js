//   引入封装好的axios
//   ps:如果没有封装，正常引入axios即可
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }
export function getList() {
  return axios.get('/sysUser/selectAllMenu')
}
export function getListOne(id) {
  return axios.get('/sysUser/selectMenuOne', {
    params: {
      id: id
    }
  })
}
/**
 * @param {Object} sysMenu 修改的系统菜单
 * @returns
 */
export function update(sysMenu) {
  return axios.post('/sysUser/updateMenu', sysMenu)
}

/**
 * @param {Object} sysMenu 新增的系统菜单
 * @returns
 */
export function insert(sysMenu) {
  return axios.post('/sysUser/insertMenu', sysMenu)
}
