//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询活动战果
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/queryAllHdzg', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

/**
 * 新增活动战果信息
 * @param {Object} Hdzg 新增数据
 * @returns
 */
export const insert = (Hdzg) => {
  return axios.post('/activity/saveHdzg', Hdzg)
}

/**
 * 修改活动战果信息
 * @param {Object} Hdzg  活动战果信息
 * @returns
 */
export const update = (Hdzg) => {
  return axios.put('/activity/updateHdzg', Hdzg)
}

/**
 * 删除活动战果信息
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/activity/delHdzg?zgbm=${idList}`)
}
/**
 * 查询单条活动战果信息
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/activity/getByIdHdzg', {
    params: { zgbm: id }
  })
}
