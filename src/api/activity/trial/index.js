//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询人员背审信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/queryAllHdjb', {
    params: paramsData
  })
}

/**
 * 根据实体信息分页查询参会人员对象
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllHdry = (paramsData) => {
  return axios.get('/activity/listIdParticipant', {
    params: paramsData
  })
}

/**
 * 根据实体类分页查询所有数据安保人员信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAllAbry = (paramsData) => {
  return axios.get('/activity/listIdSecuritypersonnel', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

// /**
//  * 查询单条参会人员特征属性
//  * @param {Number} id 编号
//  * @returns
//  */
// export const selectOneHdry = (id) => {
//   return axios.get('/activity/selectOneParticipant', {
//     params: { id: id }
//   })
// }

// /**
//  * 查询单条安保人员数据
//  * @param {Number} id 编号
//  * @returns
//  */
// export const selectOneAbry = (id) => {
//   return axios.get('/activity/selectOneSecuritypersonnel', {
//     params: { id: id }
//   })
// }

/**
 * 查询单条参会人员与安保人员特征属性
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/activity/selectOneBackgroundinfo', {
    params: { zjhm: id }
  })
}
/**
 * 修改安保人员数据
 * @param {Object} securitypersonnel  安保人员对象特征属性
 * @returns
 */
export const updateAbry = (securitypersonnel) => {
  return axios.put('/activity/updateSecuritypersonnel', securitypersonnel)
}

/**
 * 修改参会人员特征属性
 * @param {Object} participant  安保人员对象特征属性
 * @returns
 */
export const updateHdry = (participant) => {
  return axios.put('/activity/updateParticipant', participant)
}

/**
 * 查询公安机关信息api
 * @param
 * @returns
 */
export const selectAllGajg = () => {
  return axios.get('/activity/selectAllGajg')
}
