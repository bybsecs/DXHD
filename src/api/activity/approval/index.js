//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 查询大型活动信息api
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/selectAllHdjb', {
    params: paramsData
  })
}

/**
 * 查询单条大型活动信息api
 * @param {Object} hdbh 查询条件
 * @returns
 */
export const selectAllone = (hdbh) => {
  return axios.get('/activity/selectOneHdjb', {
    params: { id: hdbh }
  })
}

/**
 * 活动审批api
 * @param {object} hdjb 大型活动信息
 * @returns
 */
export const update = (hdjb) => {
  return axios.put('/activity/updateHdjb', hdjb)
}

/**
 * 查询公安机关信息api
 * @param
 * @returns
 */
export const selectAllGajg = () => {
  return axios.get('/activity/selectAllGajg')
}
