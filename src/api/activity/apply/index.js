//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 活动申请api
 * @param {Object} paramsData 活动申请数据
 * @returns
 */
export const insert = (paramsData) => {
  return axios.post('/activity/insertHdjb', paramsData)
}

/**
 * 查询公安机关信息api
 * @param
 * @returns
 */
export const selectAllGajg = () => {
  return axios.get('/activity/selectAllGajg')
}

/**
 * 上传多个文件api
 * @param {Object} hdjb 活动级别
 * @returns
 */
export const upload = (hdjb) => {
  return axios.post('/file/hdUploadMultipleFiles', hdjb)
}
