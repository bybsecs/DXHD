//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询活动档案信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/queryAllHdrzxx', {
    params: paramsData
  })
}
