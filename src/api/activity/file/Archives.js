//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询活动档案信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/selectAllHdda', {
    params: paramsData
  })
}

/**
 * 档案归档
 * @param {Object} Hdda 活动档案
 * @returns
 */
export const insert = (Hdda) => {
  return axios.post('/activity/insertHdda', Hdda)
}

// /**
//  * 修改单条安保活动信息
//  * @param {Object} abrw  活动战果信息
//  * @returns
//  */
// export const update = (abrw) => {
//   return axios.put('/activity/updateAbrw', abrw)
// }

/**
 * 删除单条活动档案
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/activity/delHdda?idList=${idList}`)
}
/**
 * 查询单条活动档案信息
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/activity/selectHddaById', {
    params: { id: id }
  })
}

/**
 * 查询各活动类型次数信息
 * @param {Number}
 * @returns
 */
export const countHdlxHdjb = () => {
  return axios.get('/activity/countHdlxHdjb')
}
