//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询场馆档案信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/selectAllCgda', {
    params: paramsData
  })
}

/**
 * 档案归档
 * @param {Object} Cgda 场馆档案
 * @returns
 */
export const insert = (Cgda) => {
  return axios.post('/activity/insertCgda', Cgda)
}

// /**
//  * 修改单条安保活动信息
//  * @param {Object} abrw  活动战果信息
//  * @returns
//  */
// export const update = (abrw) => {
//   return axios.put('/activity/updateAbrw', abrw)
// }

/**
 * 查询场馆档案echarts信息
 * @param {Object}  查询条件
 * @returns
 */
export const selectAllEcharts = () => {
  return axios.get('/activity/countCgmcHdjb')
}
