//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询安保方案
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/activity/selectAllAbrw', {
    params: paramsData
  })
}

/**
 * 新增安保活动
 * @param {Object} abrw 安保方案实体
 * @returns
 */
export const insert = (abrw) => {
  return axios.post('/activity/insertAbrw', abrw)
}

/**
 * 修改单条安保活动信息
 * @param {Object} abrw  活动战果信息
 * @returns
 */
export const update = (abrw) => {
  return axios.put('/activity/updateAbrw', abrw)
}

/**
 * 删除单条安保活动信息
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/activity/deleteAbrw?idList=${idList}`)
}
/**
 * 查询单条安保活动信息
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/activity/selectOneAbrw', {
    params: { rwbh: id }
  })
}
