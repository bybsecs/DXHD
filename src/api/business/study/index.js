//   引入封装好的axios
import axios from '@/utils/axios'
// /api为配置跨域的路径变量
// const selectAll = '/sysUser/selectAllMenu'
// export const getList = (paramsData) => {
//   return axios.get(selectAll, {
//     params: paramsData
//   })
// }

/**
 * 情报研判查询所有信息
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/intelligenceinfo/selectAll', {
    params: paramsData
  })
}

/**
 * 新增情报研判数据
 * @param {Object} intelligenceinfo 新增数据
 * @returns
 */
export const insert = (intelligenceinfo) => {
  return axios.post('/intelligenceinfo/insert', intelligenceinfo)
}

/**
 * 修改情报研判数据
 * @param {Object} intelligenceinfo 修改的数据
 * @returns
 */
export function update(intelligenceinfo) {
  return axios.put('/intelligenceinfo/update', intelligenceinfo)
}

/**
 * 查询公安机关信息api
 * @param
 * @returns
 */
export const selectAllGajg = () => {
  return axios.get('/activity/selectAllGajg')
}

/**
 * 删除情报研判数据
 * @param {Number} hdbh 活动编号
 * @returns
 */
export const delData = (hdbh) => {
  return axios.delete(`/intelligenceinfo/delete?idList=${hdbh}`)
}

/**
 * 情报来源统计
 * @param {Object}  查询条件
 * @returns
 */
export const selectAllQbly = () => {
  return axios.get('/intelligenceinfo/selectqblytj')
}
/**
 * 情报类型统计
 * @param {Object}  查询条件
 * @returns
 */
export const selectAllQblx = () => {
  return axios.get('/intelligenceinfo/selectqblxtj')
}
