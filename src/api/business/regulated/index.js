//   引入封装好的axios
import axios from '@/utils/axios'

/**
 * 查询所有地慢对象
 * @param {Object} paramsData 查询条件
 * @returns
 */
export const selectAll = (paramsData) => {
  return axios.get('/lowslowsmallaircraft/selectAll', {
    params: paramsData
  })
}

// /**
//  * 通过实体条件查询涉会车辆数据
//  * @param {Object} paramsData 查询条件
//  * @returns
//  */
// export const queryAll = (paramsData) => {
//   return axios.get('/datum/queryAllParticipantvehicle', {
//     params: paramsData
//   })
// }

/**
 * 新增地慢对象
 * @param {Object} Participant 新增数据
 * @returns
 */
export const insert = (Participant) => {
  return axios.post('/lowslowsmallaircraft/insert', Participant)
}

/**
 * 修改地慢对象
 * @param {Object} Participant 参会人员数据实体
 * @returns
 */
export const update = (Participant) => {
  return axios.put('/datum/updateParticipant', Participant)
}

/**
 * 删除涉会车辆数据
 * @param {Number} idList id集合
 * @returns
 */
export const delData = (idList) => {
  return axios.delete(`/datum/delParticipant?idList=${idList}`)
}
/**
 * 查询单条参会人员
 * @param {Number} id 编号
 * @returns
 */
export const selectOne = (id) => {
  return axios.get('/datum/selectOneParticipant', {
    params: { id: id }
  })
}
