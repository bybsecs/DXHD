import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
// import { showLoading, hideLoading } from '../components/loading'
// import Home from '../views/home'
// import Administration from '../views/administration'
// import Admin from '../views/administration/adminChildren/admin'
// import User from '../views/administration/adminChildren/user'
// import Journal from '../views/administration/adminChildren/journal'
// import Fnc from '../views/fnc'
// import Ticket from '../views/fnc/fncChildren/ticket'
// import Papers from '../views/fnc/fncChildren/papers'
// import VideoAccess from '../views/fnc/fncChildren/videoAccess'
// import Voice from '../views/fnc/fncChildren/voice/index'
// import Control from '../views/control'
// import Note from '../views/control/controlChildren/note'
// import Car from '../views/control/controlChildren/car'
// import Personnel from '../views/control/controlChildren/personnel'
// import Region from '../views/control/controlChildren/region'
// import Release from '../views/control/controlChildren/release'
// import ReleaseCar from '../views/control/controlChildren/release/releaseChildren/releaseCar'
// import ReleasePeople from '../views/control/controlChildren/release/releaseChildren/releasePeople'
// import ReleaseRegion from '../views/control/controlChildren/release/releaseChildren/releaseRegion'
// import Management from '../views/management'
// import PoliceDisplay from '../views/management/managementChildren/policeDisplay'
// import PoliceDisplayCar from '../views/management/managementChildren/policeDisplay/policeDisplayChildren/car'
// import PoliceDisplayPolic from '../views/management/managementChildren/policeDisplay/policeDisplayChildren/polic'
// import PoliceRoutes from '../views/management/managementChildren/policeRoute'
// import PoliceRoute from '../views/management/managementChildren/policeRoute/policeRouterChildren/route.vue'
// import PoliceRouteTrail from '../views/management/managementChildren/policeRoute/policeRouterChildren/trail.vue'
// import PointPeople from '../views/management/managementChildren/pointPeople'
// import PointPeopleAviation from '../views/management/managementChildren/pointPeople/pointPeopleChildren/aviation.vue'
// import PointPeopleRailway from '../views/management/managementChildren/pointPeople/pointPeopleChildren/railway.vue'
// import PointPeopleBus from '../views/management/managementChildren/pointPeople/pointPeopleChildren/bus.vue'
// import PointPeopleCheckpoint from '../views/management/managementChildren/pointPeople/pointPeopleChildren/checkpoint.vue'
// import PointPeopleHotel from '../views/management/managementChildren/pointPeople/pointPeopleChildren/hotel.vue'
// import PointPeopleShip from '../views/management/managementChildren/pointPeople/pointPeopleChildren/ship.vue'
// import PointPeopleInternetBar from '../views/management/managementChildren/pointPeople/pointPeopleChildren/internetBar.vue'
// import PointPeoplePortrait from '../views/management/managementChildren/pointPeople/pointPeopleChildren/portrait.vue'
// import PoliceMan from '../views/management/managementChildren/policeMan'
// import StressCar from '../views/management/managementChildren/stressCar'
// import StatisticsCar from '../views/management/managementChildren/stressCar/stressCarChildren/statisticsCar'
// import PositionCar from '../views/management/managementChildren/stressCar/stressCarChildren/positionCar'
// import SociologyCar from '../views/management/managementChildren/sociologyCar'
// import PoliceDatas from '../views/management/managementChildren/policeDatas'
// import PeopleTrial from '../views/management/managementChildren/peopleTrial'
// import Meeting from '../views/management/managementChildren/meeting'
// import Business from '../views/business'
// import Scene from '../views/business/businessChildren/scene'
// import Study from '../views/business/businessChildren/study'
// import Regulated from '../views/business/businessChildren/regulated'
// import CircleManagement from '../views/business/businessChildren/circleManagement'
// import Activity from '../views/activity'
// import Apply from '../views/activity/activityChildren/apply'
// import Approval from '../views/activity/activityChildren/approval'
// import Programme from '../views/activity/activityChildren/programme'
// import ProgrammeDetailed from '../views/activity/activityChildren/programme/detailed'
// import Trial from '../views/activity/activityChildren/trial'
// import File from '../views/activity/activityChildren/file'
// import ActivityPoliceMan from '../views/activity/activityChildren/man/policeMan.vue'
// import ActivityWorkMan from '../views/activity/activityChildren/man/workMan.vue'
// import ActivityArchives from '../views/activity/activityChildren/file/fileChildren/activityArchives'
// import VenueArchives from '../views/activity/activityChildren/file/fileChildren/venueArchives'
// import ActivityJournal from '../views/activity/activityChildren/file/fileChildren/activityJournal'
// import Statistics from '../views/activity/activityChildren/statistics'
// import Checkpoint from '../views/activity/activityChildren/statistics/statisticsChildren/checkpoint'
// import PreventionControl from '../views/activity/activityChildren/statistics/statisticsChildren/control'
// import Watchman from '../views/activity/activityChildren/statistics/statisticsChildren/watchman'
// import Situation from '../views/situation'
// import SituationMap from '../views/situation/situationChildren/map'
// import Login from '../views/login'
Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: '登录',
    component: () =>
      import(/* webpackChunkName: "login" */ '../views/login/index.vue')
  },
  {
    path: '/home',
    name: '首页',
    // component: Home,
    component: () =>
      import(/* webpackChunkName: "Home" */ '../views/home/index.vue')
  },
  {
    path: '/admin',
    name: '系统管理',
    // component: Administration,
    component: () =>
      import(/* webpackChunkName: "Administration" */ '../views/administration/index.vue'),
    redirect: '/admin/role',
    children: [
      {
        path: 'role',
        // component: Admin,
        component: () => import(/* webpackChunkName: "Admin" */ '../views/administration/adminChildren/admin/index.vue')
      },
      {
        path: 'user',
        // component: User,
        component: () => import(/* webpackChunkName: "User" */ '../views/administration/adminChildren/user/index.vue')
      },
      {
        path: 'journal',
        // component: Journal,
        component: () => import(/* webpackChunkName: "Journal" */ '../views/administration/adminChildren/journal/index.vue')
      }
    ]
  },
  {
    path: '/fnc',
    name: '接入功能',
    // component: Fnc,
    component: () => import(/* webpackChunkName: "Fnc" */ '../views/fnc/index.vue'),
    redirect: '/fnc/ticket',
    children: [
      {
        path: 'ticket',
        // component: Ticket,
        component: () => import(/* webpackChunkName: "Ticket" */ '../views/fnc/fncChildren/ticket/index.vue')
      },
      {
        path: 'papers',
        // component: Papers,
        component: () => import(/* webpackChunkName: "Papers" */ '../views/fnc/fncChildren/papers/index.vue')
      },
      {
        path: 'videoAccess',
        // component: VideoAccess,
        component: () => import(/* webpackChunkName: "VideoAccess" */ '../views/fnc/fncChildren/videoAccess/index.vue')
      },
      {
        path: 'Voice',
        // component: Voice,
        component: () => import(/* webpackChunkName: "Voice" */ '../views/fnc/fncChildren/voice/index.vue')
      }
    ]
  },
  {
    path: '/control',
    name: '布控管理',
    // component: Control,
    component: () => import(/* webpackChunkName: "Control" */ '../views/control/index.vue'),
    redirect: '/control/personnel',
    children: [
      {
        path: 'note',
        // component: Note,
        component: () => import(/* webpackChunkName: "Note" */ '../views/control/controlChildren/note/index.vue')
      },
      {
        path: 'car',
        // component: Car,
        component: () => import(/* webpackChunkName: "Car" */ '../views/control/controlChildren/car/index.vue')
      },
      {
        path: 'personnel',
        // component: Personnel,
        component: () => import(/* webpackChunkName: "Personnel" */ '../views/control/controlChildren/personnel/index.vue')
      },
      {
        path: 'region',
        // component: Region,
        component: () => import(/* webpackChunkName: "Region" */ '../views/control/controlChildren/region/index.vue')
      },
      {
        path: 'release',
        // component: Release,
        component: () => import(/* webpackChunkName: "Release" */ '../views/control/controlChildren/release/index.vue'),
        children:
          [
            {
              path: 'releaseCar',
              // component: ReleaseCar,
              component: () => import(/* webpackChunkName: "ReleaseCar" */ '../views/control/controlChildren/release/releaseChildren/releaseCar/index.vue')
            },
            {
              path: 'releasePeople',
              // component: ReleasePeople,
              component: () => import(/* webpackChunkName: "ReleasePeople" */ '../views/control/controlChildren/release/releaseChildren/releasePeople/index.vue')
            },
            {
              path: 'releaseRegion',
              // component: ReleaseRegion,
              component: () => import(/* webpackChunkName: "ReleaseRegion" */ '../views/control/controlChildren/release/releaseChildren/releaseRegion/index.vue')
            }
          ]
      }
    ]
  },
  {
    path: '/management',
    name: '数据管理',
    // component: Management,
    component: () => import(/* webpackChunkName: "Management" */ '../views/management/index.vue'),
    redirect: '/management/policeDisplay/policeDisplayCar',
    children: [
      {
        path: 'policeDisplay',
        // component: PoliceDisplay,
        component: () => import(/* webpackChunkName: "PoliceDisplay" */ '../views/management/managementChildren/policeDisplay/index.vue'),
        children:
          [
            {
              path: 'policeDisplayCar',
              // component: PoliceDisplayCar,
              component: () => import(/* webpackChunkName: "PoliceDisplayCar" */ '../views/management/managementChildren/policeDisplay/policeDisplayChildren/car/index.vue')
            },
            {
              path: 'policeDisplayPolic',
              // component: PoliceDisplayPolic,
              component: () => import(/* webpackChunkName: "PoliceDisplayPolic" */ '../views/management/managementChildren/policeDisplay/policeDisplayChildren/polic/index.vue')
            }
          ]
      },
      {
        path: 'policeRoutes',
        // component: PoliceRoutes,
        component: () => import(/* webpackChunkName: "PoliceRoutes" */ '../views/management/managementChildren/policeRoute/index.vue'),
        children:
          [
            {
              path: 'policeRoute',
              // component: PoliceRoute,
              component: () => import(/* webpackChunkName: "PoliceRoute" */ '../views/management/managementChildren/policeRoute/policeRouterChildren/route.vue')
            },
            {
              path: 'policeRouteTrail',
              // component: PoliceRouteTrail,
              component: () => import(/* webpackChunkName: "PoliceRouteTrail" */ '../views/management/managementChildren/policeRoute/policeRouterChildren/trail.vue')
            }
          ]
      },
      {
        path: 'pointPeople',
        // component: PointPeople,
        component: () => import(/* webpackChunkName: "PointPeople" */ '../views/management/managementChildren/pointPeople/index.vue'),
        children:
          [
            {
              path: 'pointPeopleAviation',
              // component: PointPeopleAviation,
              component: () => import(/* webpackChunkName: "PointPeopleAviation" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/aviation.vue')
            },
            {
              path: 'pointPeopleRailway',
              // component: PointPeopleRailway,
              component: () => import(/* webpackChunkName: "PointPeopleRailway" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/railway.vue')
            },
            {
              path: 'pointPeopleBus',
              // component: PointPeopleBus,
              component: () => import(/* webpackChunkName: "PointPeopleBus" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/bus.vue')
            },
            {
              path: 'pointPeopleCheckpoint',
              // component: PointPeopleCheckpoint,
              component: () => import(/* webpackChunkName: "PointPeopleCheckpoint" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/checkpoint.vue')
            },
            {
              path: 'pointPeopleHotel',
              // component: PointPeopleHotel,
              component: () => import(/* webpackChunkName: "PointPeopleHotel" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/hotel.vue')
            },
            {
              path: 'pointPeopleShip',
              // component: PointPeopleShip,
              component: () => import(/* webpackChunkName: "PointPeopleShip" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/ship.vue')
            },
            {
              path: 'pointPeopleInternetBar',
              // component: PointPeopleInternetBar,
              component: () => import(/* webpackChunkName: "PointPeopleInternetBar" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/internetBar.vue')
            },
            {
              path: 'pointPeoplePortrait',
              // component: PointPeoplePortrait,
              component: () => import(/* webpackChunkName: "PointPeoplePortrait" */ '../views/management/managementChildren/pointPeople/pointPeopleChildren/portrait.vue')
            }
          ]
      },
      {
        path: 'policeMan',
        // component: PoliceMan,
        component: () => import(/* webpackChunkName: "PoliceMan" */ '../views/management/managementChildren/policeMan/index.vue')
      },
      {
        path: 'stressCar',
        // component: StressCar,
        component: () => import(/* webpackChunkName: "StressCar" */ '../views/management/managementChildren/stressCar/index.vue'),
        children:
          [
            {
              path: 'statisticsCar',
              // component: StatisticsCar,
              component: () => import(/* webpackChunkName: "StatisticsCar" */ '../views/management/managementChildren/stressCar/stressCarChildren/statisticsCar/index.vue')
            },
            {
              path: 'positionCar',
              // component: PositionCar,
              component: () => import(/* webpackChunkName: "PositionCar" */ '../views/management/managementChildren/stressCar/stressCarChildren/positionCar/index.vue')
            }
          ]
      },
      {
        path: 'sociologyCar',
        // component: SociologyCar,
        component: () => import(/* webpackChunkName: "SociologyCar" */ '../views/management/managementChildren/sociologyCar/index.vue')
      },
      {
        path: 'policeDatas',
        // component: PoliceDatas,
        component: () => import(/* webpackChunkName: "PoliceDatas" */ '../views/management/managementChildren/policeDatas/index.vue')
      },
      {
        path: 'peopleTrial',
        // component: PeopleTrial,
        component: () => import(/* webpackChunkName: "PeopleTrial" */ '../views/management/managementChildren/peopleTrial/index.vue')
      },
      {
        path: 'meeting',
        // component: Meeting,
        component: () => import(/* webpackChunkName: "Meeting" */ '../views/management/managementChildren/meeting/index.vue')
      }
    ]
  },
  {
    path: '/business',
    name: '业务协同',
    // component: Business,
    component: () => import(/* webpackChunkName: "Business" */ '../views/business/index.vue'),
    redirect: '/business/scene',
    children: [
      {
        path: 'scene',
        // component: Scene,
        component: () => import(/* webpackChunkName: "Scene" */ '../views/business/businessChildren/scene/index.vue')
      },
      {
        path: 'study',
        // component: Study,
        component: () => import(/* webpackChunkName: "Study" */ '../views/business/businessChildren/study/index.vue')
      },
      {
        path: 'regulated',
        // component: Regulated,
        component: () => import(/* webpackChunkName: "Regulated" */ '../views/business/businessChildren/regulated/index.vue')
      },
      {
        path: 'circleManagement',
        // component: CircleManagement,
        component: () => import(/* webpackChunkName: "CircleManagement" */ '../views/business/businessChildren/circleManagement/index.vue')
      }
    ]
  },
  {
    name: '活动管理',
    path: '/activity',
    // component: Activity,
    component: () => import(/* webpackChunkName: "Activity" */ '../views/activity/index.vue'),
    redirect: '/activity/apply',
    children: [
      {
        path: 'apply',
        // component: Apply,
        component: () => import(/* webpackChunkName: "Apply" */ '../views/activity/activityChildren/apply/index.vue')
      },
      {
        path: 'approval',
        // component: Approval,
        component: () => import(/* webpackChunkName: "Approval" */ '../views/activity/activityChildren/approval/index.vue')
      },
      {
        path: 'programme',
        // component: Programme,
        component: () => import(/* webpackChunkName: "Programme" */ '../views/activity/activityChildren/programme/index.vue')
      },
      {
        path: 'programmes',
        name: 'programmes',
        // component: ProgrammeDetailed,
        component: () => import(/* webpackChunkName: "ProgrammeDetailed" */ '../views/activity/activityChildren/programme/detailed/index.vue')
      },
      {
        path: 'Trial',
        // component: Trial,
        component: () => import(/* webpackChunkName: "Trial" */ '../views/activity/activityChildren/trial/index.vue')
      },
      {
        path: 'file',
        // component: File,
        component: () => import(/* webpackChunkName: "File" */ '../views/activity/activityChildren/file/index.vue'),
        children: [
          {
            path: 'activityArchives',
            // component: ActivityArchives,
            component: () => import(/* webpackChunkName: "ActivityArchives" */ '../views/activity/activityChildren/file/fileChildren/activityArchives/index.vue')
          },
          {
            path: 'venueArchives',
            // component: VenueArchives,
            component: () => import(/* webpackChunkName: "VenueArchives" */ '../views/activity/activityChildren/file/fileChildren/venueArchives/index.vue')
          },
          {
            path: 'activityJournal',
            // component: ActivityJournal,
            component: () => import(/* webpackChunkName: "ActivityJournal" */ '../views/activity/activityChildren/file/fileChildren/activityJournal/index.vue')
          }
        ]
      },
      {
        path: 'statistics',
        // component: Statistics,
        component: () => import(/* webpackChunkName: "Statistics" */ '../views/activity/activityChildren/statistics/index.vue'),
        children: [
          {
            path: 'checkpoint',
            // component: Checkpoint,
            component: () => import(/* webpackChunkName: "Checkpoint" */ '../views/activity/activityChildren/statistics/statisticsChildren/checkpoint/index.vue')
          },
          {
            path: 'control',
            // component: PreventionControl,
            component: () => import(/* webpackChunkName: "PreventionControl" */ '../views/activity/activityChildren/statistics/statisticsChildren/control/index.vue')
          },
          {
            path: 'watchman',
            // component: Watchman,
            component: () => import(/* webpackChunkName: "Watchman" */ '../views/activity/activityChildren/statistics/statisticsChildren/watchman/index.vue')
          }
        ]
      },
      {
        path: 'ActivityPoliceMan/:id',
        // component: ActivityPoliceMan,
        component: () => import(/* webpackChunkName: "ActivityPoliceMan" */ '../views/activity/activityChildren/man/policeMan.vue')
      },
      {
        path: 'ActivityMan/:id',
        // component: ActivityWorkMan,
        component: () => import(/* webpackChunkName: "ActivityWorkMan" */ '../views/activity/activityChildren/man/workMan.vue')
      }
    ]
  },
  {
    name: '态势感知',
    path: '/situation',
    // component: Situation,
    component: () => import(/* webpackChunkName: "ActivityWorkMan" */ '../views/situation/index.vue'),
    redirect: '/situation/situationMap',
    children: [
      {
        path: 'situationMap',
        // component: SituationMap,
        component: () => import(/* webpackChunkName: "SituationMap" */ '../views/situation/situationChildren/map/index.vue')
      }
    ]
  }
]

const router = new VueRouter({
  // mode: 'history',
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  store.commit('setRouterName', to.matched[0].name)
  store.commit('setCurrentMenu', to.path)
  document.title = to.matched[0].name
  // showLoading()
  // setTimeout(() => {
  //   hideLoading()
  // }, 888)
  next()
  // console.log('%c ', 'background: url(https://pic4.zhimg.com/v2-bb497f3b4eba1e7ec95b4b859bc51054_r.jpg?source=1940ef5chttps://pic4.zhimg.com/v2-bb497f3b4eba1e7ec95b4b859bc51054_r.jpg?source=1940ef5c) no-repeat center;padding-left:800px;padding-bottom: 500px;background-size: 100% 100%;')
  // console.log('%c ', 'background: url(https://pic1.zhimg.com/v2-edd414d01c4c0d838f5fd6530c91ea09_r.jpg?source=1940ef5c) no-repeat center;padding-left:800px;padding-bottom: 500px;background-size: 100% 100%;')
})
export default router
