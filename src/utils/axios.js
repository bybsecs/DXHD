import Axios from 'axios'
import router from '@/router/index'
import NProgress from 'nprogress'
import { Message } from 'element-ui'

// import Cookies from 'js-cookie'

const axios = Axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? '/api' : 'http://pm2.winvers.com/zhdxhd',
  timeout: process.env.NODE_ENV === 'development' ? 60 * 1000 : 10 * 1000
})

const toLogin = () => {
  router.push({
    path: '/login',
    query: {
      // 把当前的路由传递过去 登录成功后从哪来返回哪去
      backRouter: router.currentRoute.fullPath
    }
  })
}
// 请求拦截器
axios.interceptors.request.use(
  config => {
    NProgress.start()
    // if (localStorage.getItem('token')) {
    //   config.headers.Authorization = localStorage.getItem('token')
    // }
    console.log(config)

    return config
    // console.log("请求成功拦截器");
    // console.log(config);
    // if (config.url !== "login") {
    //   // 创建token
    //   // Cookies.set("token", "aaaaaaaaaaaaaaaaaaaa");
    //   // config.headers.token = Cookies.get("token"); // 获取token
    //   // 删除token
    //   // Cookies.remove("token");
    // }
    // 必须返回 config
    // return config
  },
  // 失败的拦截，一般是发送不到的
  err => {
    // console.log("请求失败拦截器");
    console.log(err)
    return Promise.reject(err)
  }
)
// 响应拦截器
axios.interceptors.response.use(
  config => {
    NProgress.done()
    if (config.status === 200) {
      Message({
        type: 'success',
        message: config.data.message,
        showClose: true,
        duration: 2000
      })
      return config.data
    } else {
      Message({
        type: 'error',
        message: config.data.message,
        showClose: true,
        duration: 2000
      })
      console.log('请求失败', config)
    }
  },
  error => {
    const index = error.message.indexOf('timeout')
    if (index > -1) {
      // eslint-disable-next-line prefer-promise-reject-errors
      return Promise.reject('请求超时了，请重试！')
    }
    const statusCode = error.response.status
    if (statusCode) {
      switch (statusCode) {
        case 401:
          Message({
            type: 'error',
            message: '暂无权限，请先登录哦！',
            showClose: true,
            duration: 2000
          })
          toLogin()
          break
        case 403:
          Message({
            type: 'error',
            message: '登录过期，请重新登录哦！',
            showClose: true,
            duration: 2000
          })
          localStorage.removeItem('token')
          toLogin()
          break
        case 500:
          Message({
            type: 'error',
            message: '服务器异常！',
            showClose: true,
            duration: 2000
          })
          break
        default:
          console.log(error.code)
          break
      }
    }
  }
  // res => {
  //   // console.log(res);
  //   // 处理所有响应
  //   return res.data
  // },
  // err => {
  //   console.log(err)
  // }
)

export default axios
