import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    routerName: '',
    currentMenu: '',
    routes: []
  },
  mutations: {
    setRouterName (state, name) {
      state.routerName = name
    },
    updateRoutes (state, routes) {
      state.routes = routes
    },
    setCurrentMenu (state, path) {
      state.currentMenu = '/' + path.split('/')[1]
    }
  },
  actions: {
  },
  modules: {
  }
})
