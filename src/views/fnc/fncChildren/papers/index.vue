<template>
  <div class="papers">
    <Elsearch :searchSettings="searchSettings" :searchBtn="searchBtn" :formData="formData" @search="search"></Elsearch>
    <Eltable :tableHead="tableHead" :tableDatas="tableDatas" :tableSettings="tableSettings" @del="del" @detailed="detailed" @sizeChange="sizeChange" @pageChange="pageChange" :pagination="pagination" :iSloading="iSloading"></Eltable>
    <Dialog :title="title" :isShow="isShow" :readOnly="readOnly" :dialogs="dialogs" @close="close" :isDetermine='isDetermine' :dialogData="dialogData" ref="dialog" ></Dialog>
  </div>
</template>
<script>
import Elsearch from '@/components/search'
import Eltable from '@/components/table'
import Dialog from '@/components/dialog'
import { selectAll, delData } from '@/api/fnc/papers/index'
export default {
  data: () => ({
    title: '',
    isShow: false,
    readOnly: true,
    isDetermine: true, // 是否显示dialog底部按钮
    iSloading: true, // 是否显示表格加载状态
    pagination: { // 分页数据
      current: 1,
      size: 10,
      total: 30
    },
    paramsData: {},
    formData: { // 查询输入框搜索数据
      hdbh: '',
      zjxxGmsfhm: '',
      zjxxLxdh: '',
      zjxxXm: ''
    },
    dialogData: { // 弹出框数据
      xxzjbh: 1,
      hdbh: '',
      zjxxGpbh: '',
      zjxxGpsj: '',
      zjxxXm: '',
      zjxxGmsfhm: '',
      zjxxLxdh: '',
      zjxxSfrc: '',
      zjxxRcsj: ''
    },
    searchSettings: [
      { placeholder: '活动编号', type: 'input', name: 'hdbh' },
      { placeholder: '身份证号码', type: 'input', name: 'zjxxGmsfhm' },
      { placeholder: '姓名', type: 'input', name: 'zjxxXm' },
      { placeholder: '请输入联系电话', type: 'input', name: 'zjxxLxdh' }
    ],
    searchBtn: [
      { name: '查询', type: 'search' }
    ],
    tableHead: [
      // { label: '序号', prop: 'input' },
      { label: '信息主键编号', prop: 'xxzjbh' },
      { label: '活动编号', prop: 'hdbh' },
      { label: '证件编号', prop: 'zjxxGpbh' },
      { label: '制证时间', prop: 'zjxxGpsj' },
      { label: '姓名', prop: 'zjxxXm' }
    ],
    tableDatas: [],
    tableSettings: [
      { name: '详情', type: 'detailed' },
      { name: '删除', type: 'delete' }
    ],
    dialogs: [
      { label: '信息主键编号', type: 'number', name: 'xxzjbh' },
      { label: '姓名', type: 'input', name: 'zjxxXm' },
      { label: '活动编号', type: 'input', name: 'hdbh' },
      { label: '联系电话', type: 'input', name: 'zjxxLxdh' },
      // { label: '证件信息', type: 'input' },
      { label: '公民身份证号码', type: 'input', name: 'zjxxGmsfhm' },
      { label: '证件编号', type: 'input', name: 'zjxxGpbh' },
      { label: '是否入场', type: 'select', name: 'zjxxSfrc', options: [{ label: '是', value: '1' }, { label: '否', value: '0' }] },
      { label: '入场时间', type: 'datetime', name: 'zjxxSfrc' },
      { label: '制证时间', type: 'datetime', name: 'zjxxGpsj' }
    ]
  }),
  created () {
    this.selectAll()
  },
  methods: {
    del (row) {
      this.$confirm('此操作将永久删除该条数据, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        delData(row.xxzjbh).then(res => {
          if (res.code === 1) {
            this.$message({
              type: 'success',
              message: '删除成功！',
              showClose: true,
              duration: 1000
            })
            this.selectAll()
          } else {
            this.$message({
              type: 'error',
              message: res.message,
              showClose: true,
              duration: 1000
            })
          }
        })
      }).catch(() => {
        this.$message({
          type: 'info',
          message: '已取消删除'
        })
      })
    },
    close () {
      this.isShow = false
      this.$refs.dialog.$refs.dialogForm.resetFields()
    },
    // 切换当前一页展示多少条
    sizeChange(val) {
      this.pagination.size = val
      delete this.pagination.total
      this.selectAll()
    },
    // 翻页
    pageChange(val) {
      this.pagination.current = val
      this.selectAll()
    },
    async selectAll() {
      const page = {}
      page.size = this.pagination.size
      page.current = this.pagination.current
      const { data: res } = await selectAll({ ...page })
      this.tableDatas = res.records
      this.pagination.total = res.total
      this.iSloading = false
    },
    async search(formData) {
      this.iSloading = true
      const page = {}; const form = { ...formData }
      page.size = this.pagination.size
      page.current = this.pagination.current
      const paramsData = { ...form, ...this.page }
      const { data: res } = await selectAll(paramsData)
      this.tableDatas = res.records
      this.pagination.total = res.total
      Object.assign(this.$data.formData, this.$options.data().formData)
      this.iSloading = false
    },
    detailed (data) {
      this.dialogData = { ...data }
      this.title = '制证信息接入详情'
      this.readOnly = true
      this.isDetermine = false
      this.isShow = true
    }
  },
  components: { Elsearch, Eltable, Dialog }
}
</script>
<style lang="scss">
.papers {
  width: 100%;
  padding-right: 0.2rem;
  box-sizing: border-box;
  .table {
    margin-top: 0.2rem;
  }
}
</style>
